import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaerdoMenuComponent } from './maerdo-menu.component';

describe('MaerdoMenuComponent', () => {
  let component: MaerdoMenuComponent;
  let fixture: ComponentFixture<MaerdoMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaerdoMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaerdoMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
